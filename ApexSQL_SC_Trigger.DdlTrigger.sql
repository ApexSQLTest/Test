USE [AdventureWorks2014]
GO
/****** Object:  DdlTrigger [ApexSQL_SC_Trigger]    Script Date: 4/20/2016 1:47:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [ApexSQL_SC_Trigger] ON DATABASE 
FOR 
DDL_ASSEMBLY_EVENTS, 
DDL_APPLICATION_ROLE_EVENTS, 
DDL_ASYMMETRIC_KEY_EVENTS, 
DDL_CERTIFICATE_EVENTS,
DDL_ROLE_EVENTS, 
DDL_SCHEMA_EVENTS, 
DDL_SYMMETRIC_KEY_EVENTS, 
DDL_USER_EVENTS, 
DDL_DEFAULT_EVENTS, 
DDL_EVENT_NOTIFICATION_EVENTS,
DDL_FULLTEXT_CATALOG_EVENTS, 
DDL_FULLTEXT_STOPLIST_EVENTS, 
DDL_FUNCTION_EVENTS, 
DDL_PARTITION_EVENTS, 
DDL_PROCEDURE_EVENTS,
DDL_RULE_EVENTS,
DDL_SEARCH_PROPERTY_LIST_EVENTS, 
DDL_SEQUENCE_EVENTS,  
DDL_CONTRACT_EVENTS, 
DDL_MESSAGE_TYPE_EVENTS, 
DDL_QUEUE_EVENTS, 
DDL_REMOTE_SERVICE_BINDING_EVENTS, 
DDL_ROUTE_EVENTS, 
DDL_SERVICE_EVENTS, 
DDL_SYNONYM_EVENTS, 
DDL_TABLE_EVENTS, 
DDL_VIEW_EVENTS, 
DDL_TRIGGER_EVENTS,
DDL_TYPE_EVENTS,
DDL_XML_SCHEMA_COLLECTION_EVENTS,
DDL_EXTENDED_PROPERTY_EVENTS,
DDL_INDEX_EVENTS,
RENAME -- Applies to sp_rename and also renaming of columns
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @data XML;
    DECLARE @schema sysname;
    DECLARE @object sysname;
    DECLARE @eventType sysname;
    DECLARE @parentObjectName nvarchar(255);
    DECLARE @parentObjectType nvarchar(255);
    DECLARE @newObjectName nvarchar(255);
    DECLARE @objectType nvarchar(50);
    DECLARE @objectName nvarchar(255);
    DECLARE @schemaName nvarchar(255);
    DECLARE @renameObject bit;
    DECLARE @currentUser nvarchar(255);

    SET @data = EVENTDATA();
    SET @eventType = @data.value('(/EVENT_INSTANCE/EventType)[1]', 'sysname');
    SET @schema = @data.value('(/EVENT_INSTANCE/SchemaName)[1]', 'sysname');
    SET @object = @data.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname');
    SET @parentObjectName = @data.value('(/EVENT_INSTANCE/TargetObjectName)[1]', 'nvarchar(255)');
    SET @parentObjectType = @data.value('(/EVENT_INSTANCE/TargetObjectType)[1]', 'nvarchar(255)');
    SET @newObjectName = @data.value('(/EVENT_INSTANCE/NewObjectName)[1]', 'nvarchar(255)');
    SET @objectType = @data.value('(/EVENT_INSTANCE/ObjectType)[1]', 'nvarchar(50)');

    IF(@eventType = 'RENAME' AND @data.value('(/EVENT_INSTANCE/ObjectType)[1]', 'nvarchar(50)') = 'COLUMN') 
	BEGIN
	    SET @renameObject = 0 
	    SET @objectName = CONVERT(nvarchar(255), @parentObjectName)				
	END
    ELSE IF(@eventType = 'RENAME')
	BEGIN
	    SET @renameObject = 1 
	    SET @objectName = CONVERT(nvarchar(255), @object)
	END
    ELSE 	   
	   SET @objectName = CONVERT(nvarchar(255), @object)
	   SET @schemaName = CONVERT(nvarchar(255), @schema)

    IF @object IS NOT NULL
        PRINT '  ' + @eventType + ' - ' + @schema + '.' + @object;
    ELSE
        PRINT '  ' + @eventType + ' - ' + @schema;

    IF @eventType IS NULL
        PRINT CONVERT(nvarchar(max), @data);

    IF @schemaName IS NULL
	SET @schemaName = ''
   
    SET @currentUser = UPPER(CONVERT(nvarchar(255), SYSTEM_USER));
    SET @objectType = UPPER(REPLACE(@objectType,' ',''))

    DECLARE @checkoutBeforeEdit bit;
    DECLARE @lockBeforeEdit bit;
    DECLARE @isLogEnabled bit;

    DECLARE @isObjectDropped bit;

    IF @eventType like 'DROP%'
	   SET @isObjectDropped = 1
    ELSE
	   SET @isObjectDropped = 0

    SET @checkoutBeforeEdit = (SELECT CheckOutBeforeEdit FROM [AdventureWorks2014].dbo.ApexSQL_SC_PoliciesTable WHERE DatabaseID = DB_ID())
    IF @checkoutBeforeEdit IS NULL SET @checkoutBeforeEdit = 0
    SET @lockBeforeEdit = (SELECT LockBeforeEdit FROM [AdventureWorks2014].dbo.ApexSQL_SC_PoliciesTable WHERE DatabaseID = DB_ID())
    IF @lockBeforeEdit IS NULL SET @lockBeforeEdit = 0       
    SET @isLogEnabled = (SELECT LogChanges FROM [AdventureWorks2014].dbo.ApexSQL_SC_PoliciesTable WHERE DatabaseID = DB_ID())
   
    IF @eventType like 'CREATE%'
	   BEGIN
		  --Object with the name already exists in the objects table (because we do not delete objects on DROP)
		  IF EXISTS(SELECT ObjectName FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID())
			 BEGIN
				UPDATE [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable
					   SET [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsCheckedOut = @checkoutBeforeEdit,
						  [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsLocked = @lockBeforeEdit,
						  [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsEdited = 1,
						  [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsDropped = 0					       
					   WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()
			 END
		  --New object is created, just set all policies to default
		  ELSE
			 BEGIN
				INSERT INTO [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable VALUES(@objectName, @schemaName, DB_ID(), @objectType, @checkoutBeforeEdit, @lockBeforeEdit, 1, @currentUser, 0)
			 END
		  
		  --New object is created, dont do anything else but logging
		  IF @isLogEnabled = 1
			 BEGIN
				INSERT into [AdventureWorks2014].dbo.ApexSQL_SC_LogTable
				VALUES
				    (
				    GETDATE(), 
				    @currentUser, 
				    @eventType, 
				    @schemaName, 
				    DB_NAME(),
				    @objectName,
				    @objectType,
				    @newObjectName,
				    @parentObjectName,
				    @parentObjectType,
				    @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(max)'), 
				    @data
				    );
			 END
		  RETURN
	   END

    DECLARE @checkedOut bit;
    DECLARE @isLocked bit;   
    DECLARE @isLockedByOtherUser bit;
    DECLARE @userName nvarchar(50);
       
    --check if locked\checked out by current user
    SET @checkedOut = (SELECT IsCheckedOut FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @objectName AND ObjectType LIKE
  CASE WHEN @objectType = N'COLUMN' THEN 
    @parentObjectType 
  ELSE
    @objectType
  END AND SchemaName = @schemaName AND DatabaseID = DB_ID() AND DatabaseUser = @currentUser) 
    SET @isLocked = (SELECT IsLocked FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID() AND DatabaseUser = @currentUser)
    
    IF @isLocked IS NULL
	   BEGIN
		  SET @isLockedByOtherUser = (SELECT IsLocked FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID())
		  SET @userName = (SELECT DatabaseUser FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID())
	   END

    DECLARE @err varchar(500)
    --Allow edit if object is locked by the same user or checked out by the same user
    IF (@lockBeforeEdit = 1 AND @isLocked = 1) OR (@lockBeforeEdit = 0 AND @checkoutBeforeEdit = 1 AND @checkedOut = 1)
	   BEGIN		  
		  UPDATE [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable
		  SET [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsEdited = 1, [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.DatabaseUser = @currentUser, [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsDropped = @isObjectDropped
		  WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()

		  IF  (@renameObject = 1) 	
			 BEGIN
				--This case covers the case when objects are being recreated
				--delete the temporary object and continue working with the original one(this is why we do not delete objects on DROP)
				IF EXISTS(SELECT ObjectName FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @newObjectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID())
				    BEGIN
					   DELETE FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()

					   UPDATE [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable
					       SET  [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsCheckedOut = @checkoutBeforeEdit,
						    [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsLocked = @lockBeforeEdit,
						    [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsEdited = 1,
						    [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsDropped = 0
					       WHERE ObjectName = @newObjectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()
				    END
				--Simple rename
				ELSE
				    BEGIN
					   UPDATE [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable
					   SET [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.ObjectName = @newObjectName, [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsDropped = 0
					   WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()
				    END				
			 END	
			 									
		  IF @isLogEnabled = 1
			 BEGIN
				INSERT into [AdventureWorks2014].dbo.ApexSQL_SC_LogTable
				VALUES
				    (
				    GETDATE(), 
				    @currentUser, 
				    @eventType, 
				    @schemaName, 
				    DB_NAME(),
				    @objectName,
				    @objectType,
				    @newObjectName,
				    @parentObjectName,
				    @parentObjectType,
				    @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(max)'), 
				    @data
				    );
			 END
	   END
    ELSE IF @isLockedByOtherUser = 1
		  BEGIN			 
			 SET @err = 'ApexSQL Source Control - Object is locked by ' + @username			 
			 RAISERROR (@err, 16, 1);
			 ROLLBACK;
		  END
    ELSE IF @lockBeforeEdit = 1 AND (@isLocked IS NULL OR @isLocked = 0)
		  BEGIN			 
			 SET @err = 'ApexSQL Source Control - Restrictive policy is enabled on this database. Object ' + @objectName  + ' must be locked before editing.'			 
			 RAISERROR (@err, 16, 1);
			 ROLLBACK;
		  END
    ELSE IF @checkoutBeforeEdit = 1 and (@checkedOut IS NULL OR @checkedOut = 0)
		  BEGIN			 			
			 SET @err = 'ApexSQL Source Control - Permissive policy is enabled on this database. Object ' + @objectName  + ' must be checked out before editing.'
			 RAISERROR (@err, 16, 1);
			 ROLLBACK;
		  END
    ELSE 
	   BEGIN
		  UPDATE [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable
			 SET [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsEdited = 1, [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.DatabaseUser = @currentUser, [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsDropped = @isObjectDropped
			 WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()

		  IF  (@renameObject = 1) 	
			 BEGIN
				--This case covers the case when objects are being recreated
				--delete the temporary object and continue working with the original one(this is why we do not delete objects on DROP)
				IF EXISTS(SELECT ObjectName FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @newObjectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID())
				    BEGIN
					   DELETE FROM [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()

					   UPDATE [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable
					       SET  [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsCheckedOut = @checkoutBeforeEdit,
						    [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsLocked = @lockBeforeEdit,
						    [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsEdited = 1,
						    [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsDropped = 0
					       WHERE ObjectName = @newObjectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()
				    END
				--Simple rename
				ELSE
				    BEGIN
					   UPDATE [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable
					   SET [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.ObjectName = @newObjectName, [AdventureWorks2014].dbo.ApexSQL_SC_ObjectsTable.IsDropped = 0
					   WHERE ObjectName = @objectName AND ObjectType = @objectType AND SchemaName = @schemaName AND DatabaseID = DB_ID()
				    END				
			 END	
			 									
		  IF @isLogEnabled = 1
			BEGIN
				INSERT into [AdventureWorks2014].dbo.ApexSQL_SC_LogTable
				VALUES
				    (
				    GETDATE(), 
				    @currentUser, 
				    @eventType, 
				    @schemaName, 
				    DB_NAME(),
				    @objectName,
				    @objectType,
				    @newObjectName,
				    @parentObjectName,
				    @parentObjectType,
				    @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(max)'), 
				    @data
				    );
			END
	   END
      
END;
GO
DISABLE TRIGGER [ApexSQL_SC_Trigger] ON DATABASE
GO
ENABLE TRIGGER [ApexSQL_SC_Trigger] ON DATABASE
GO
EXEC sys.sp_addextendedproperty @name=N'ApexSQL Description', @value=N'Framework objs are placed in: [AdventureWorks2014]' , @level0type=N'TRIGGER',@level0name=N'ApexSQL_SC_Trigger'
GO
